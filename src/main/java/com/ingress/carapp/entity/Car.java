package com.ingress.carapp.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String model;

    @Column(name = "year_of_car")
    private int year;


    private double price;
    private String color;

    @Column(name = "condition_of_car")
    private boolean condition;

   // @Column(name = "max_speed")
   // private double maxSpeed;
}
