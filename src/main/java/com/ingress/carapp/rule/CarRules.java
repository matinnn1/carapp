package com.ingress.carapp.rule;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CarRules {
    public void checkYear(int year) {
        if (year == 0)
            throw new RuntimeException("Car year not declared");

        else if (year > LocalDateTime.now().getYear())
            throw new RuntimeException("Car year must be minimum current year");
    }
}
