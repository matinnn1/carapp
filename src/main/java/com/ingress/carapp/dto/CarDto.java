package com.ingress.carapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarDto {
    private int id;
    private String model;
    private int year;
    private double price;
    private String color;
    private boolean condition;
    //private double max_speed;
}
