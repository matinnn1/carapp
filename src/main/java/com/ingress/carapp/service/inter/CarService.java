package com.ingress.carapp.service.inter;

import com.ingress.carapp.dto.CarDto;

import java.util.List;

public interface CarService {
    List<CarDto> findAll();
    CarDto findById(int id);
    CarDto add(CarDto carDto);
    CarDto update(int id, CarDto carDto);
    void deleteById(int id);
}
