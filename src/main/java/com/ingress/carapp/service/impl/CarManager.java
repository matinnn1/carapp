package com.ingress.carapp.service.impl;

import com.ingress.carapp.dto.CarDto;
import com.ingress.carapp.entity.Car;
import com.ingress.carapp.mapper.CarMapper;
import com.ingress.carapp.repository.CarRepository;
import com.ingress.carapp.rule.CarRules;
import com.ingress.carapp.service.inter.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarManager implements CarService {
    private final CarRepository carRepository;
    private final CarRules carRules;
    private final CarMapper carMapper;
    @Override
    public List<CarDto> findAll() {
        List<Car> cars = carRepository.findAll();
        return cars.stream().map((car) -> carMapper.entityToDto(car)).collect(Collectors.toList());
    }

    @Override
    public CarDto findById(int id) {
        var car = carRepository.findById(id).orElseThrow(() -> new RuntimeException("Car not found"));

        return carMapper.entityToDto(car);
    }

    @Override
    public CarDto add(CarDto carDto) {
        carRules.checkYear(carDto.getYear());
        Car createdCar = carRepository.save(carMapper.dtoToEntity(carDto));

        return carMapper.entityToDto(createdCar);
    }

    @Override
    public CarDto update(int id, CarDto carDto) {
        var car = carRepository.findById(id).orElseThrow(() -> new RuntimeException("Car not found"));

        car = carMapper.dtoToEntity(carDto);

        car = carRepository.save(car);

        return carMapper.entityToDto(car);
    }

    @Override
    public void deleteById(int id) {
        carRepository.deleteById(id);
    }
}
