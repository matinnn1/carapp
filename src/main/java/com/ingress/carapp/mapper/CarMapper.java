package com.ingress.carapp.mapper;

import com.ingress.carapp.dto.CarDto;
import com.ingress.carapp.entity.Car;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CarMapper {
    CarDto entityToDto(Car car);
    Car dtoToEntity(CarDto carDto);
}
