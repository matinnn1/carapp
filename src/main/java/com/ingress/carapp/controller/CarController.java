package com.ingress.carapp.controller;

import com.ingress.carapp.dto.CarDto;
import com.ingress.carapp.service.inter.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/car")
@RequiredArgsConstructor
public class CarController {

    private final CarService carService;

    @GetMapping()
    public List<CarDto> getALl() {
        return carService.findAll();
    }

    @GetMapping("/{id}")
    public CarDto getById(@PathVariable int id) {
        return carService.findById(id);
    }

    @PostMapping()
    public CarDto addUser(@RequestBody CarDto userDto) {
        return carService.add(userDto);
    }

    @PutMapping("{id}")
    public CarDto updateUser(@PathVariable int id, @RequestBody CarDto userDto) {
        return carService.update(id, userDto);
    }

    @DeleteMapping("/{id}")
    public void deleteUserById(@PathVariable int id) {
        carService.deleteById(id);
    }
}
