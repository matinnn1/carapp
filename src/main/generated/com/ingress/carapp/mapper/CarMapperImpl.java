package com.ingress.carapp.mapper;

import com.ingress.carapp.dto.CarDto;
import com.ingress.carapp.entity.Car;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-27T10:13:22+0400",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class CarMapperImpl implements CarMapper {

    @Override
    public CarDto entityToDto(Car car) {
        if ( car == null ) {
            return null;
        }

        CarDto.CarDtoBuilder carDto = CarDto.builder();

        carDto.id( car.getId() );
        carDto.model( car.getModel() );
        carDto.year( car.getYear() );
        carDto.price( car.getPrice() );
        carDto.color( car.getColor() );
        carDto.condition( car.isCondition() );

        return carDto.build();
    }

    @Override
    public Car dtoToEntity(CarDto carDto) {
        if ( carDto == null ) {
            return null;
        }

        Car.CarBuilder car = Car.builder();

        car.id( carDto.getId() );
        car.model( carDto.getModel() );
        car.year( carDto.getYear() );
        car.price( carDto.getPrice() );
        car.color( carDto.getColor() );
        car.condition( carDto.isCondition() );

        return car.build();
    }
}
