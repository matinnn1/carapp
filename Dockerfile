FROM openjdk:17-alpine
COPY build/libs/*.jar /app/
WORKDIR /app/
RUN mv /app/*.jar /app/car-app.jar
ENTRYPOINT ["java","-jar","/app/car-app.jar"]